---
title: Kafka 基础概念
date: 2020-05-17
updated: 2020-05-17
categories: 
   - Kafka
tags:
   - Kafka
---

# Kafka 基础概念
Kafka是一个基于发布/订阅模式的消息队列，主要用于大数据实时处理领域

[TOC]

---

## 1. 消息队列（Message Queue）

1. 点对点模式（一对一，消费者主动拉取数据，消息收到后消息清除）

   - 消息生产者生产消息发送到Queue中，消费者从Queue中取出并消费消息
   - 息消费后不再存储
   - Queue支持多个消费者

   ![image-20200515170741253](Kafka 基础概念.assets/image-20200515170741253.png)

2. 发布/订阅模式（一对多，消费者消费数据后不会清除数据）
   - 消息生产者（发布）将消息发布到topic中，同时有多个消息消费者（订阅）消费该消息
   - topic中的消息会被所有的订阅者消费
   
   ![image-20200515170845988](Kafka 基础概念.assets/image-20200515170845988.png)
   
##  2. Kafka 架构

   Kafka 属于发布/订阅模型，整体架构如下所示：

   ![image-20200515170945608](Kafka 基础概念.assets/image-20200515170945608.png)

   

- Producer：消息发布者
- Consumer：消息消费者
- Consumer Group：消费者组，有多个消费者构成。
  - 消费者组内的每个消费者负责消费不同分区的数据
  - 一个分区只能由消费者组中的一个消费者消费
  - 消费者组之间互不影响。
  - 所有的消费者都属于某个消费者组，即消费者组是逻辑上的一个订阅者
- Broker：一台Kafka 服务器就是一个broker。
  - 一个集群由多个broker组成
  - 一个broker可以容纳多个topic
- Topic：可以理解为一个消息队列，生产者和消费者面向的都是一个topic
- Partition：为了实现扩展性，一个非常大的topic可以分布到多个broker（即服务器）上，一个topic可以分为多个partition，每一个partition都是一个有序队列
- Replica：副本。为了保证集群中某个节点出现故障时，该节点上的partition数据不丢失，且Kafka 可以继续工作，Kafka 提供副本机制，一个topic的每个分区都有若干个副本，一个leader和若干个follower
- leader：每个分区多个副本的“主”，生产者发送数据的对象，以及消费者消费数据的对象都是leader
- follower：每个分区多个副本的“从”，实时从leader中同步数据。leader发生故障时，某个follower会成为新的leader

## 3. Topic 分区

对于每一个topic，Kafka集群都会维持一个分区日志，如下所示：

![image-20200515171207810](Kafka 基础概念.assets/image-20200515171207810.png)

1. 每个分区都是有序且不可变的记录集，并且不断追加到结构化的commit log文件。
2. 分区中的每一个记录都会分配一个id号表示顺序，即offset，offset用来唯一的标识分区中的每一条记录
3. Kafka每个分区中的数据严格有序，但是多分区之间不能确保有序

> Kafka中partition的疑惑：
>
> 在Kafka中，一个topic可以被分为多个partition，首先在大数据的前提之下，一个topic 中可能会存在多个数据，如果没有进行partition划分，那么所有的数据都会存储在一个broker中（即一台服务器中），这样broker就成为了瓶颈限制（数据大小的限制、生产者写入数据的限制、消费者读取数据的限制），如果将topic数据分布到整个集群中，为了避免这个限制，需要将数据进行分布式存储，即将topic存储在多个topic上，即partition的概念，这样每一个partition中的数据在逻辑上是等效的，无论消费者从哪一个partition中订阅数据，都需要对数据一同样的逻辑尽进行处理

##4. Kafka 存储机制

在Kafka中，topic是一个逻辑上的概念，而partition是物理上的概念，具体的逻辑结构如下所示：

![image-20200515171644206](Kafka 基础概念.assets/image-20200515171644206.png)

一个逻辑上的topic有多个partition组成，每一个partition都对应一个log文件，该log文件存储的就是producer生产的数据，producer生产的数据会源源不断的追加到log文件的末端。同时每条数据有自己的offset。消费者组中的每一个消费者会记录自己消费到哪一个offset。

同时为了防止log文件过大导致offset定位数据效率低下，Kafka采用**分片**和**索引**机制，将一个partition分为多个segment，每一个segment对应一个.index文件和.log文件。

| 类别   | 作用                                         |
| ------ | -------------------------------------------- |
| .index | offset索引文件，存储着offset对应的数据偏移量 |
| .log   | 日志文件，存储生产者生产的数据               |

![image-20200515171827567](Kafka 基础概念.assets/image-20200515171827567.png)

> 生产者生产的每一条消息的大小都是未知的，因此在.log中占据的大小也是未知的，所以需要在.index文件中将消息的offset和.log文件中具体位置相对应。

.index文件和.log文件都是以当前segment的第一条消息的偏移量offset命名。

> 偏移量offset是一个64位的长整形数，固定是20位数字，长度未达到，用0进行填补，索引文件和日志文件都由该作为文件名命名规则。所以从上图可以看出，我们的偏移量是从0开始的，.index和.log文件名称都为00000000000000000000。
>
> 具体的偏移量offset的计算是文件名+segment内部存储的索引值的大小，例如偏移量offset为7的时候，其在00000000000000000006.index中，是00000000000000000006.index中索引值为1对应的内容，即198。

partition以文件夹的形式存在，其命名规则为：topic名称-分区序号。所有的segment文件都存在该文件夹，

## 5. Kafka 生产者

### 5.1 topic的分区策略

无论是消息生产者还是消息消费者都是面向topic的，topic又可以分为多个partition，那么生产者是如何将消息发送到partition中？

Kafka在发送消息的过程中遵守以下的规则：

1. 指明partition的情况下，直接将消息发送到指定的partition上
2. 没有指明partition，但是声明了key，将key的hash值与topic的partition总数求余得到partition的值
3. 既没有指明partition，也没有指明key，则在第一次调用的时候随机生成一个整数值，将这个值与topic的partition总数求余得到partition，之后每一次调用时将整数值自增

## 6. 可靠性保证

topic的每一个partition收到producer发送到的消息之后都需要向producer发送ACK，如果producer收到ACK，就进行下一轮发送，否则重新发送

同时Kafka为了应对不同的情况，为用户提供了三种可靠性级别：

- 0： producer不等待broker的ACK。该操作提供了一个最低的延迟，但是当broker故障的时候可能丢失数据
- 1： producer等待broker的ACK。在leader将数据存储之后返回ACK，但是如果在follower同步成功之前leader故障，则有可能丢失数据
- -1： producer等待leader和follower全部完成数据存储之后返回ACK，但是如果在follower完成同步之后、leader发送ACK之前，leader故障，则会造成数据重复（producer没有收到ACK会重发数据）

### 6.1 ISR

在kafka副本同步的时候，leader会等待所有的副本同步完成才会发送ACK给producer，但是如果有一个follower因为某种原因，迟迟不能与leader完成同步，就会将leader一直阻塞，为了解决这种问题，kafka提出了ISR（in-sync replica set，和leader保持同步的集合）。

当ISR中的所有的follower完成同步之后，leader就会发送ACK给producer，但是如果一个follower长时间未完成同步，就会将该follower踢出ISR中。

在leader发生故障之后，就会从ISR中选举新的leader

### 6.2 数据一致性问题

这里的数据一致性问题主要是指leader和follower之间的数据不一致问题，并不能保证kafka集群中每时每刻follower的长度都是和leader一致的（同步时候的数据延迟），因此kafka使用了如下的机制确保即使在follower长度不一致的情况下也可以保证高可用：

 ![image-20200515172354807](Kafka 基础概念.assets/image-20200515172354807.png)

这里kafka引入了两个新的概念：

- LEO（Long End Offset）：指的是每一个副本最后一个offset
- HW（High Wather）：指的是消费者能看见的最大的offset，是ISR队列中最小的LEO

针对这个规则：

- 当follower发生故障时：follower 发生故障后会被临时踢出 ISR，待该 follower 恢复后，follower 会读取本地磁盘记录的上次的 HW，并将 log 文件高于 HW 的部分截取掉，从 HW 开始向 leader 进行同步。等该 follower 的 LEO 大于等于该 Partition 的 HW，即 follower 追上 leader 之后，就可以重新加入 ISR 了。
- 当leader发生故障时：leader 发生故障之后，会从 ISR 中选出一个新的 leader，之后，为保证多个副本之间的数据一致性，其余的 follower 会先将各自的 log 文件高于 HW 的部分截掉，然后从新的 leader 同步数据。

### 6.3 Exactly One

- At Least Once：将broker的ACK级别设置为-1，可以保证producer到broker的之间不会丢失数据，但是可能存在数据重复
- At Mast Once：将broker的ACK级别设置为0，可以保证生产者每条消息只会被发送一次，但是无法保证数据不会丢失。
- Exactly Once：数据不能丢失也不能重复

0.11之后的kafka引入了一个重大的特性：幂等性。所谓的幂等性保证无论 Producer 不论 向 Server 发送多少次重复数据，Server 端都只会持久化一条。幂等性结合 At Least Once 语义，就构成了 Kafka 的 Exactly Once 语义。

要启用幂等性，只需要将 Producer 的参数中 enable.idompotence 设置为true即可。Kafka的幂等性实现其实就是将原来下游需要做的去重放在了数据上游。开启幂等性的 Producer 在初始化的时候会被分配一个 PID，发往同一 Partition 的消息会附带 Sequence Number。而 Broker 端会对<PID, Partition, SeqNumber>做缓存，当具有相同主键的消息提交时，Broker 只 会持久化一条。

> 但是 Producer重启后，其 PID 就会变化，同时不同的 Partition 也具有不同主键，所以幂等性无法保证跨 分区跨会话的 Exactly Once。

### 6.4 生产者事务

Kafka 从 0.11 版本开始引入了事务支持。事务可以保证 Kafka 在 Exactly Once 语义的基础上，生产和消费可以跨分区和会话，要么全部成功，要么全部失败。

为了实现跨分区跨会话的事务，需要引入一个全局唯一的 Transaction ID，并将 Producer获得的PID 和Transaction ID 绑定。这样当Producer 重启后就可以通过正在进行的 Transaction ID 获得原来的 PID。

为了管理 Transaction，Kafka 引入了一个新的组件 Transaction Coordinator。Producer 就是通过和 Transaction Coordinator 交互获得 Transaction ID 对应的任务状态。Transaction Coordinator 还负责将事务所有写入 Kafka 的一个内部 Topic，这样即使整个服务重启，由于事务状态得到保存，进行中的事务状态可以得到恢复，从而继续进行。

## 7. Kafka 消费者

Kafka消费者采用pull模式从broker中消费数据。

> push模式中消息发送的速率由broker决定，其目标是尽快的将消息进行传递，容易造成consumer来不及处理消息。
>
> 而pull模式则可以根据 consumer 的消费能力以适当的速率消费消息。

pull 模式不足之处是，如果 Kafka 没有数据，消费者可能会陷入循环中，一直返回空数据。针对这一点，Kafka 的消费者在消费数据时会传入一个时长参数 timeout，如果当前没有数据可供消费，consumer 会等待一段时间之后再返回，这段时长即为 timeout。

### 7.1 分区策略

#### 7.1.1 Range策略

Range策略是Kafka默认采取的consumer分区策略，具体的内容如下如图所示：

![image-20200515180314828](Kafka 基础概念.assets/image-20200515180314828.png)

在多topic的订阅中，Kafka不会将多个主题看作一个整体。

![image-20200515180411600](Kafka 基础概念.assets/image-20200515180411600.png)

#### 7.1.2 RoundRobin策略

RoundRobin即轮询的意思，具体的分配策略如下：

![image-20200515180601265](Kafka 基础概念.assets/image-20200515180601265.png)

需要注意的是在多个topic订阅的时候，Kafka会将所有的partition看作一个整体，如下所示：

![image-20200515180657763](Kafka 基础概念.assets/image-20200515180657763.png)

### 7.2 offset维护

offset由消息的主题Topic+分区Partition和消费者组名称唯一确定。

Kafka 0.9 版本之前，consumer 默认将 offset 保存在 Zookeeper 中，从 0.9 版本开始， consumer 默认将 offset 保存在 Kafka 一个内置的 topic 中，该 topic 为__consumer_offsets。

# 参考文章

1. [Mrbird - Kafka入门](https://mrbird.cc/Kafka入门.html)
2. [Mrbird - Kafka存储机制](https://mrbird.cc/Kafka存储机制.html)
3. [Mrbird - Kafka生产者](https://mrbird.cc/Kafka生产者.html)
